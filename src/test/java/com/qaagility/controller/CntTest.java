package com.qaagility.controller;

import static org.mockito.Mockito.*;
import org.junit.Test;
import static org.junit.Assert.*;


public class CntTest {

	@Test
	public void testCnt() throws Exception {
		int result = new Cnt().d(18,2);
		assertEquals("New Test Failed not 9",result, 9);
	}

        @Test
        public void testCntBZero() throws Exception {
                int result = new Cnt().d(18,0);
                assertEquals("New Test Failed not 9",result, Integer.MAX_VALUE);
        }


}
